export default async function({ store, redirect }) {
  if (store.getters["user/isGuest"]) {
    await store.dispatch("user/getUserData").catch(err => redirect("/"));
  }
}
