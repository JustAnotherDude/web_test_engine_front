module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: "web_test_engine_front",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Nuxt.js project" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: "#3B8070" },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
    vendor: ["marked"],
  },

  router: {
    middleware: "auth",
  },

  css: ["assets/main.css"],

  modules: ["@nuxtjs/vuetify", "@nuxtjs/axios"],

  vuetify: {
    theme: {
      primary: "#2962FF",
      secondary: "#26A69A",
      accent: "#9c27b0",
      error: "#f44336",
      warning: "#ffeb3b",
      info: "#2196f3",
      success: "#4caf50",
    },
  },
  axios: {
    baseURL: process.env.API_URL || "http://localhost:3001",
    credentials: true,
  },
};
