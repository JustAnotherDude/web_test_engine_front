export const requestTo = async (app, path, error, redirect) => {
  const res = await app.$axios.$get(path).catch(({ response: { status } }) => {
    if (status === 404) {
      error({ statusCode: 404 });
    } else {
      redirect("/");
    }
  });

  return res;
};
