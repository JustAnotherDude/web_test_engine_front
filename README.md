# Web Test Engine Front

> Project for Alexander Muskin (The Pidor).

## Build Setup

```bash
# install dependencies
$ yarn # Or "npm i"

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```
